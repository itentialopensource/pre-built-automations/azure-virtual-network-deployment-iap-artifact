<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your pre-built name -->
# Azure Virtual Network Deployment IAP Pre-Built

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
The Azure Virtual Network Deployment pre-built enables users of the Itential Automation Platform to create custom virtual networks on Microsoft Azure. The pre-built allows users to select custom networking components such as subnets (public and private), routes & route tables. 
<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<table><tr><td>
  <img src="./images/Azure-Virtual-Network-Deployment.png" alt="Auzure V-Net Deployment Workflow" width="800px">
</td></tr></table>

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 2 minutes

## Installation Prerequisites
In order to use this pre-built, users will have to satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.2`

## Requirements

<!-- Unordered list highlighting the requirements of the pre-built -->
This pre-built requires the following:
* Access to a Microsoft Azure account 
* Azure Adapter instance
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

* Provides a form that allows users to create Virtual Networks (V-Nets) 
* Created V-Nets can contain networking components such as subnets (public and private), routes & route tables
* Zero-touch operation mode executes automation end to end without manual tasks
<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements

<!-- OPTIONAL - Mention if the pre-built will be enhanced with additional features on the road map -->
<!-- Ex.: This pre-built would support Cisco XR and F5 devices -->
Provide support to attach Virtual Network Gateway for the virtual network

## How to Install

Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 

The pre-built can be installed from within Pre-builts catalog from Admin Essentials. Simply search for the name of your desired pre-built and click the install button as shown below:

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED PRE_BUILT -->
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->


<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

<!-- Explain the main entrypoint(s) for this pre-built: Automation Catalog item, Workflow, Postman, etc. -->
Use the following to run the pre-built:


This pre-built may be run from Operations Manager by clicking `Run` for the Virtual Network Form item. The user will be presented with a form to select options to customize the V-Net that will be created.

* Select the Azure Adapter instance to use with the automation. 
* Select the location to determine the region within which the V-Net will be created.
* Select a Resource Group. This list can be updated with the names from the adapter configuration.
* Select a CIDR Block. Enter a IPv4 CIDR IPv4 address with mask on the end with values from 0 - 32.

<table><tr><td>
  <img src="./images/Azure-Virtual-Network-Form.png" alt="Azure V-Net Form" width="600px">
</td></tr></table>

Within the form, users are able to create subnets and select whether they are private or public. 

<table><tr><td>
  <img src="./images/Azure-Virtual-Network-Form-Subnets.png" alt="Azure V-Net Subnets" width="600px">
</td></tr></table>

Below, users are also able to Customize Routes and Route Tables. 

<table><tr><td>
  <img src="./images/Azure-Virtual-Network-Form-Route-Table.png" alt="Azure V-Net Route Table" width="600px">
</td></tr></table>

### Create Network 

This workflow creates or updates a Network within the specified region. 

<table><tr><td>
  <img src="./images/Azure-Virtual-Network-Create.png" alt="Azure Create Network" width="600px">
</td></tr></table>

### Create Subnet

This workflow creates a subnet in the specified virtual network. 

<table><tr><td>
  <img src="./images/Azure-Subnet.png" alt="Azure Create Subnet" width="600px">
</td></tr></table>

### Create Route

This workflow creates Routes or Route Tables within the specified resource group. 

<table><tr><td>
  <img src="./images/Azure Route Table CRUD.png" alt="Azure Create Route Table" width="600px">
</td></tr></table>


## Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
