
## 0.2.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!25

---

## 0.2.4 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!24

---

## 0.2.3 [02-14-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!23

---

## 0.2.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!22

---

## 0.2.1 [03-18-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!20

---

## 0.2.0 [07-16-2020]

* [minor/LB-404] Add './' to img path

See merge request itentialopensource/pre-built-automations/azure-virtual-network-deployment-iap-artifact!14

---

## 0.1.0 [06-17-2020]

* Update files to reflect correct naming convention

See merge request itentialopensource/pre-built-automations/Azure-Virtual-Network-Deployment-IAP-Artifact!13

---

## 0.0.5 [05-04-2020]

* Update package.json, package-lock.json, manifest.json, artifact.json,...

See merge request itentialopensource/pre-built-automations/azure_virtual_network_deployment_iap_artifact!11

---

## 0.0.4 [04-29-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/azure_virtual_netwok_deployment_iap_artifact!6

---

## 0.0.3 [04-28-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/azure_virtual_netwok_deployment_iap_artifact!7

---

## 0.0.2 [04-27-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/azure_virtual_netwok_deployment_iap_artifact!5

---\n\n
